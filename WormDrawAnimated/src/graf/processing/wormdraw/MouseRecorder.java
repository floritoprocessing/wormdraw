package graf.processing.wormdraw;

import java.util.Date;
import java.util.Vector;

import processing.core.*;

public class MouseRecorder {
	
	int numberOfFrames = 0;
	int currentFrame = 0;
	
	int[] mouseX, mouseY;
	boolean[] mousePressed;
	
	public static final int MAX_NUMBER_OF_FRAMES = 50000;
	
	String firstLine;
	String filename;
	private static final String FILE_STRING_SHOOTERWORM_SETTINGS = "BEGIN SHOOTER_WORM_SETTINGS";
	private static final String FILE_STRING_SHOOTER_SETTINGS = "BEGIN SHOOTER SETTINGS";
	private static final String FILE_STRING_MOUSE_DATA = "BEGIN SHOOTER DATA";
	
	private static final int DATATYPE_SHOOTERWORM = 0;
	private static final int DATATYPE_SHOOTER = 1;
	private static final int DATATYPE_MOUSE = 2;
	
	/**
	 * @param filename
	 * @param numberOfFrames
	 */
	public MouseRecorder(String filename) {
		this.filename = filename;
		this.numberOfFrames = MAX_NUMBER_OF_FRAMES;//numberOfFrames;
		mouseX = new int[numberOfFrames];
		mouseY = new int[numberOfFrames];
		mousePressed = new boolean[numberOfFrames];
	}
	
	public void setFilename(String filename) {
		this.filename = filename;
	}
	
	public void initRecording(ShooterWorm shooterWorm) {
		this.firstLine = FILE_STRING_SHOOTERWORM_SETTINGS + "\r\n";
		this.firstLine += shooterWorm.toString() + "\r\n";
		this.firstLine += FILE_STRING_SHOOTER_SETTINGS + "\r\n";
		this.firstLine += shooterWorm.allPositionsToString();
		this.firstLine += FILE_STRING_MOUSE_DATA;
		currentFrame=0;
	}
	
	/**
	 * Records mouseX, mouseY and mouseDown. Returns false if recording has ended
	 * @param mouseX
	 * @param mouseY
	 * @param mousePressed
	 * @return
	 */
	public boolean record(int mouseX, int mouseY, boolean mousePressed) {
		if (currentFrame<numberOfFrames) {
			this.mouseX[currentFrame] = mouseX;
			this.mouseY[currentFrame] = mouseY;
			this.mousePressed[currentFrame] = mousePressed;
			currentFrame++;
			return true;
		} else {
			return false;
		}
	}
	
	public int getCurrentFrame() {
		return currentFrame;
	}
	
	public void endRecording(PApplet pa) {
		numberOfFrames = currentFrame;
		saveRecording(pa);
	}
	
	public void saveRecording(PApplet pa) {
		System.out.println("saving to "+filename);
		String[] data = new String[currentFrame+1];
		data[0] = firstLine;
		for (int i=0;i<currentFrame;i++) {
			data[i+1] = ""+mouseX[i]+","+mouseY[i]+","+(mousePressed[i]?1:0);
		}
		pa.saveStrings(filename, data);
		System.out.println("done");
	}

	
	public void initPlaying() {
		currentFrame = 0;
	}
	
	public void nextFrame() {
		currentFrame++;
	}
	
	public boolean afterLastFrame() {
		return currentFrame>=numberOfFrames;
	}
	
	public int getMouseXAtFrame(int i) {
		return mouseX[i];
	}
	
	public int getCurrentMouseX() {
		return mouseX[currentFrame];
	}
	
	public int getMouseYAtFrame(int i) {
		return mouseY[i];
	}
	
	public int getCurrentMouseY() {
		return mouseY[currentFrame];
	}
	
	public boolean getMousePressedAtFrame(int i) {
		return mousePressed[i];
	}
	
	public boolean getCurrentMousePressed() {
		return mousePressed[currentFrame];
	}
	
	
	private float getFloatData(String line) {
		return Float.parseFloat(line.split(" ")[1]);
	}
	
	public ShooterWorm loadRecording(PApplet pa) {
		System.out.println("loading "+filename);
		String[] data = pa.loadStrings(filename);
		int dataType = -1;
		
		int amount = 0; 
		float drag = 0, gravity = 0, hardness = 0, pressureFollow = 0, pull = 0;
		
		int currentShooter = 0;
		float[] x = new float[ShooterWorm.SHOOTER_AMOUNT_MAX];
		float[] y = new float[ShooterWorm.SHOOTER_AMOUNT_MAX];
		float[] xm = new float[ShooterWorm.SHOOTER_AMOUNT_MAX];
		float[] ym = new float[ShooterWorm.SHOOTER_AMOUNT_MAX];
		float[] pressure = new float[ShooterWorm.SHOOTER_AMOUNT_MAX];
		boolean[] active = new boolean[ShooterWorm.SHOOTER_AMOUNT_MAX];
		boolean[] visible = new boolean[ShooterWorm.SHOOTER_AMOUNT_MAX];
		
		Vector mouseData = new Vector();
		
		for (int i=0;i<data.length;i++) {
			if (data[i].equals(FILE_STRING_SHOOTERWORM_SETTINGS)) dataType=DATATYPE_SHOOTERWORM;
			else if (data[i].equals(FILE_STRING_SHOOTER_SETTINGS)) dataType=DATATYPE_SHOOTER;
			else if (data[i].equals(FILE_STRING_MOUSE_DATA)) dataType=DATATYPE_MOUSE;
			else {
				
				if (dataType==DATATYPE_SHOOTERWORM) {
					if (data[i].startsWith(ShooterWorm.VARNAME_AMOUNT)) amount = (int)getFloatData(data[i]);
					if (data[i].startsWith(ShooterWorm.VARNAME_DRAG)) drag = getFloatData(data[i]);
					if (data[i].startsWith(ShooterWorm.VARNAME_GRAVITY)) gravity = getFloatData(data[i]);
					if (data[i].startsWith(ShooterWorm.VARNAME_HARDNESS)) hardness = getFloatData(data[i]);
					if (data[i].startsWith(ShooterWorm.VARNAME_PRESSURE_FOLLOW)) pressureFollow = getFloatData(data[i]);
					if (data[i].startsWith(ShooterWorm.VARNAME_PULL)) pull = getFloatData(data[i]);
				}
				
				if (dataType==DATATYPE_SHOOTER) {
					String[] col = data[i].split(",");
					for (int j=0;j<col.length;j++) {
						if (col[j].startsWith(Shooter.VARNAME_X)) x[currentShooter] = getFloatData(col[j]);
						if (col[j].startsWith(Shooter.VARNAME_Y)) y[currentShooter] = getFloatData(col[j]);
						if (col[j].startsWith(Shooter.VARNAME_XM)) xm[currentShooter] = getFloatData(col[j]);
						if (col[j].startsWith(Shooter.VARNAME_YM)) ym[currentShooter] = getFloatData(col[j]);
						if (col[j].startsWith(Shooter.VARNAME_PRESSURE)) pressure[currentShooter] = getFloatData(col[j]);
						if (col[j].startsWith(Shooter.VARNAME_ACTIVE)) active[currentShooter] = getFloatData(col[j])==1;
						if (col[j].startsWith(Shooter.VARNAME_VISIBLE)) visible[currentShooter] = getFloatData(col[j])==1;
					}
					currentShooter++;
				}
				
				if (dataType==DATATYPE_MOUSE) {
					if (data[i].length()>0) mouseData.add(data[i]);
				}
				
			}
			
		}
		
		numberOfFrames = mouseData.size();
		mouseX = new int[numberOfFrames];
		mouseY = new int[numberOfFrames];
		mousePressed = new boolean[numberOfFrames];
		
		for (int i=0;i<mouseData.size();i++) {
			String line = (String)mouseData.elementAt(i);
			String[] col = line.split(",");
			mouseX[i] = Integer.parseInt(col[0]);
			mouseY[i] = Integer.parseInt(col[1]);
			mousePressed[i] = Integer.parseInt(col[2])==1;
		}
		
		
		return new ShooterWorm(
				amount, pull, drag, pressureFollow, gravity, hardness,
				x, y, xm, ym, pressure, active, visible);
	}

	public int getNumberOfFrames() {
		return numberOfFrames;
	}

	public void setNumberOfFrames(int numberOfFrames) {
		this.numberOfFrames = numberOfFrames;
	}

	public static MouseRecorder getInterpolated(MouseRecorder m1, MouseRecorder m2, double d) {
		float p = (float)d;
		MouseRecorder m = new MouseRecorder("nada");
		m.currentFrame = 0;
		m.numberOfFrames = m1.numberOfFrames;
		for (int i=0;i<m.numberOfFrames;i++) {
			m.mouseX[i] = (int)(m1.mouseX[i] + p * (m2.mouseX[i]-m1.mouseX[i]));
			m.mouseY[i] = (int)(m1.mouseY[i] + p * (m2.mouseY[i]-m1.mouseY[i]));
			float mp1 = m1.mousePressed[i]?1:0;
			float mp2 = m2.mousePressed[i]?1:0;
			float mp = mp1 + p * (mp2-mp1);
			m.mousePressed[i] = mp>=0.5;
		}
		/*int numberOfFrames = 0;
		int currentFrame = 0;
		int[] mouseX, mouseY;
		boolean[] mousePressed;*/
		return m;
	}

	
	

}
