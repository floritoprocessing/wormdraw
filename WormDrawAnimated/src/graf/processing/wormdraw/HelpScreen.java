package graf.processing.wormdraw;

import processing.core.*;

public class HelpScreen {

	private boolean VISIBLE = false;
	private String FONT_LOAD_NAME = "CourierNewPSMT-11.vlw";
	private PFont font;
	private KeyInfo keyInfo;
	private ShooterWorm shooterWorm;

	private String STATUS = "";
	private static final int STATUS_TIME = 3000;
	private int statusTime = 0;

	private PApplet pa;

	HelpScreen(PApplet pa, KeyInfo ki, ShooterWorm sw) {
		keyInfo = ki;
		shooterWorm = sw;
		this.pa = pa;
		font = pa.loadFont(FONT_LOAD_NAME);
	}

	void setVisible(boolean v) {
		VISIBLE = v;
	}

	boolean getVisible() {
		return VISIBLE;
	}

	void setStatus(String s) {
		STATUS = s;
		statusTime = pa.millis();
	}

	void showStatus() {
		if (!STATUS.equals("")) {
			// show:
			pa.stroke(255,255,255);
			pa.fill(128,128,128);
			pa.rectMode(PApplet.CORNER);
			pa.rect(10,pa.height-30,240,20);
			pa.fill(0,0,0);
			pa.textFont(font,11);
			pa.text(STATUS,20,pa.height-15);
			if (pa.millis()>statusTime+STATUS_TIME) STATUS="";
		}
	}

	void update() {
		if (VISIBLE) {
			pa.stroke(255,255,255);
			pa.fill(128,128,128);
			pa.rectMode(PApplet.CORNER);
			pa.rect(10,10,240,320);

			pa.fill(0,0,0);
			pa.textAlign(PApplet.LEFT);
			pa.textFont(font,11);
			pa.text("KEY BINDINGS:",20,30);

			String[] asText = keyInfo.getAsText();
			for (int i=0;i<asText.length;i++) {
				pa.text(asText[i],20,50+i*10);
			}

			pa.text("SETTINGS:",20,70+asText.length*10);
			pa.text("pull:           "+PApplet.nf(shooterWorm.getPull(),1,5),20,70+asText.length*10+20);
			pa.text("drag:           "+PApplet.nf(shooterWorm.getDrag(),1,5),20,70+asText.length*10+30);
			pa.text("gravity:        "+PApplet.nf(shooterWorm.getGravity(),1,5),20,70+asText.length*10+40);
			pa.text("hardness:       "+PApplet.nf(shooterWorm.getHardness(),1,5),20,70+asText.length*10+50);
			pa.text("pressureFollow: "+PApplet.nf(shooterWorm.getPressureFollow(),1,5),20,70+asText.length*10+60);
			pa.text("amount:         "+(int)shooterWorm.getAmount(),20,70+asText.length*10+70);

		}

		showStatus();
	}

}
