package graf.processing.wormdraw;

import processing.core.*;

public class ShooterWorm {

	public static final String VARNAME_AMOUNT = "amount";
	public static final String VARNAME_PULL = "pull";
	public static final String VARNAME_DRAG = "drag";
	public static final String VARNAME_PRESSURE_FOLLOW = "pressureFollow";
	public static final String VARNAME_GRAVITY = "gravity";
	public static final String VARNAME_HARDNESS = "hardness";
	
	private int PRESET = 0;
	private int PRESET_AMOUNT = 6;

	public static final int SHOOTER_AMOUNT_MAX = 200;

	private int[] SHOOTER_AMOUNT = { 
			30 , 30 , 60 , 80 , 70 , 50       };
	private float[] SHOOTER_PULL = { 
			0.05000f, 0.03000f, 0.33000f, 0.40000f, 0.80000f, 0.56352f    };
	private float[] SHOOTER_DRAG = { 
			0.22000f, 0.20000f, 0.50000f, 0.45000f, 0.58000f, 0.66743f    };
	private float[] SHOOTER_PRESSURE_FOLLOW = { 
			0.15000f, 0.15000f, 0.35000f, 0.75000f, 0.40000f, 0.37751f    };
	private float[] SHOOTER_GRAVITY = {
			0.00000f, 0.00000f, 0.00000f, 0.00000f,-1.00000f, 0.39951f    };
	private float[] SHOOTER_HARDNESS = {
			0.04000f, 0.04000f, 0.04000f, 0.04000f, 0.04000f, 0.04000f     };

	private int amountOfShooters;
	private float pull;
	private float drag;
	private float pressureFollow;
	private float gravity;
	private float hardness;

	private boolean VISIBLE = true;

	Shooter[] shooter;

	ShooterWorm() {

		shooter = new Shooter[SHOOTER_AMOUNT_MAX];
		for (int i=0;i<shooter.length;i++) {

			shooter[i] = new Shooter(this);

			if (i<SHOOTER_AMOUNT[PRESET]) shooter[i].setActive();
			else shooter[i].setInactive();
			if (i==0) shooter[i].setTarget(Shooter.TARGET_MOUSE); 
			else shooter[i].setTarget(shooter[i-1]);

		}

		amountOfShooters = SHOOTER_AMOUNT[PRESET];
		pull = SHOOTER_PULL[PRESET];
		drag = SHOOTER_DRAG[PRESET];
		pressureFollow = SHOOTER_PRESSURE_FOLLOW[PRESET];
		gravity = SHOOTER_GRAVITY[PRESET];
		hardness = SHOOTER_HARDNESS[PRESET];
	}

	ShooterWorm(
			int amount, float pull, float drag, float pressureFollow, float gravity, float hardness, 
			float[] x, float[] y, float[] xm, float ym[], float[] pressure, boolean[] active, boolean[] visible) {
		shooter = new Shooter[SHOOTER_AMOUNT_MAX];

		this.amountOfShooters = amount;
		this.pull = pull;
		this.drag = drag;
		this.pressureFollow = pressureFollow;
		this.gravity = gravity;
		this.hardness = hardness;

		for (int i=0;i<shooter.length;i++) {
			shooter[i] = new Shooter(this,x[i],y[i],xm[i],ym[i],pressure[i],active[i],visible[i]);
		}
		for (int i=0;i<shooter.length;i++) {
			if (i==0) shooter[i].setTarget(Shooter.TARGET_MOUSE); 
			else shooter[i].setTarget(shooter[i-1]);
		}
	}

	

	float getPull() {
		return pull;
	}

	float getDrag() {
		return drag;
	}

	float getGravity() {
		return gravity;
	}

	float getHardness() {
		return hardness;
	}

	float getPressureFollow() {
		return pressureFollow;
	}

	float getAmount() {
		return amountOfShooters;
	}

	void addShooter() {
		amountOfShooters++;
		shooter[(int)amountOfShooters-1].setActive();
	}

	void removeShooter() {
		if (amountOfShooters>0) {
			amountOfShooters--;
			shooter[(int)amountOfShooters].setInactive();
		}
	}
	
	public Shooter getShooterAt(int i) {
		return shooter[i];
	}

	void randomizePreset0() {
		amountOfShooters = (int)(Math.random()*SHOOTER_AMOUNT_MAX); //SHOOTER_AMOUNT[PRESET];
		pull = (float)Math.random(); //SHOOTER_PULL[PRESET];
		drag = (float)Math.random(); //SHOOTER_DRAG[PRESET];
		pressureFollow = (float)Math.random(); //SHOOTER_PRESSURE_FOLLOW[PRESET];
		gravity = (float)(-1 + 2*Math.random());//ER_GRAVITY[PRESET];
		hardness = (float)Math.random();//SHOOTER_HARDNESS[PRESET];
	}

	public String toString() {
		String out = "" +
		VARNAME_AMOUNT + " " + amountOfShooters + "\r\n" +
		VARNAME_PULL + " " + pull + "\r\n" +
		VARNAME_DRAG + " " + drag + "\r\n" + 
		VARNAME_PRESSURE_FOLLOW + " " + pressureFollow + "\r\n" + 
		VARNAME_GRAVITY + " " + gravity + "\r\n" +
		VARNAME_HARDNESS + " " + hardness;
		return out;
	}

	void setVisible(boolean v) {
		VISIBLE = v;
		for (int i=0;i<shooter.length;i++) if (VISIBLE) shooter[i].setVisible(); 
		else shooter[i].setInvisible();
	}

	boolean getVisible() {
		return VISIBLE;
	}

	public void setPressure(float pressure) {
		for (int i=0;i<shooter.length;i++) shooter[i].setPressure(pressure);
	}


	void setPreset(int p) {
		pull = SHOOTER_PULL[p];
		drag = SHOOTER_DRAG[p];
		pressureFollow = SHOOTER_PRESSURE_FOLLOW[p];
		gravity = SHOOTER_GRAVITY[p];
		amountOfShooters = SHOOTER_AMOUNT[p];
		hardness = SHOOTER_HARDNESS[p];
		for (int i=0;i<shooter.length;i++) {
			if (i<amountOfShooters) shooter[i].setActive();
			else shooter[i].setInactive();
		}
	}

	void reset(int mouseX, int mouseY) {
		for (int i=0;i<shooter.length;i++) shooter[i].reset(mouseX,mouseY);
	}

	void draw(PApplet pa,int mouseX,int mouseY,boolean mousePressed,PImage32 img) {
		for (int i=0;i<shooter.length;i++) shooter[i].update(mouseX,mouseY,mousePressed);
		for (int i=0;i<shooter.length;i++) shooter[shooter.length-i-1].draw(pa,img);
	}

	public String allPositionsToString() {
		String out = "";
		for (int i=0;i<shooter.length;i++)
			out += ""+shooter[i].toString() + "\r\n";
		return out;
	}

	public static ShooterWorm getInterpolated(ShooterWorm worm1, ShooterWorm worm2, double d) {
		float p = (float)d;
		
		int amount = (int)(worm1.getAmount() + p * (worm2.getAmount() - worm1.getAmount()));
		float pull = worm1.getPull() + p * (worm2.getPull() - worm1.getPull());
		float drag = worm1.getDrag() + p * (worm2.getDrag() - worm1.getDrag());
		float pressureFollow = worm1.getPressureFollow() + p * (worm2.getPressureFollow() - worm1.getPressureFollow());
		float gravity = worm1.getGravity() + p * (worm2.getGravity() - worm1.getGravity());
		float hardness = worm1.getHardness() + p * (worm2.getHardness() - worm1.getHardness());
		
		float[] x=new float[SHOOTER_AMOUNT_MAX], y=new float[SHOOTER_AMOUNT_MAX], 
				xm=new float[SHOOTER_AMOUNT_MAX], ym=new float[SHOOTER_AMOUNT_MAX], 
				pressure=new float[SHOOTER_AMOUNT_MAX];
		boolean[] active=new boolean[SHOOTER_AMOUNT_MAX], visible=new boolean[SHOOTER_AMOUNT_MAX];
		
		int lastActive1 = -1, lastActive2 = -1;
		int lastVisible1 = -1, lastVisible2 = -1;
		for (int i=0;i<SHOOTER_AMOUNT_MAX;i++) {
			Shooter s1 = worm1.getShooterAt(i);
			Shooter s2 = worm2.getShooterAt(i);
			x[i] = s1.getX() + p * (s2.getX() - s1.getX());
			y[i] = s1.getY() + p * (s2.getY() - s1.getY());
			xm[i] = s1.getXM() + p * (s2.getXM() - s1.getXM());
			ym[i] = s1.getYM() + p * (s2.getYM() - s1.getYM());
			pressure[i] = s1.getPressure() + p * (s2.getPressure()-s1.getPressure());
			if (s1.getVisible()) lastVisible1 = i;
			if (s2.getVisible()) lastVisible2 = i;
			if (s1.getActive()) lastActive1 = i;
			if (s2.getActive()) lastActive2 = i;
		}
		
		int lastActive = (int)(lastActive1 + p * (lastActive2 - lastActive1));
		int lastVisible = (int)(lastVisible1 + p * (lastVisible2 - lastVisible1));
		
		for (int i=0;i<SHOOTER_AMOUNT_MAX;i++) {
			if (i<=lastActive) active[i]=true; else active[i]=false;
			if (i<=lastVisible) visible[i]=true; else visible[i]=false;
		}
		
		return new ShooterWorm(
				amount, pull, drag, pressureFollow, gravity, hardness,
				x, y, xm, ym, pressure, active, visible);
	}



}
