package graf.processing.wormdraw;

import processing.core.*;

public class WormDrawAnimated extends PApplet {

	public static void main(String[] args) {
		PApplet.main(new String[] {"graf.processing.wormdraw.WormDrawAnimated"});
	}

	String BASE_NAME = "D:\\Processing Output\\WormDrawAnimated\\Anim";
	String EXTENSION = ".tab";

	KeyInfo keyInfo;
	ShooterWorm shooterWorm;
	PImage32 screenImage;//, undoImage;
	HelpScreen helpScreen;
	MouseRecorder mouseRecorder;
	
	ShooterWorm worm1, worm2;
	MouseRecorder mouse1, mouse2;

	boolean recording = false;
	boolean playing = false;
	boolean interpolating = false;
	
	int INTERPOLATION_FRAMES = 100;
	int currentInterpolationFrame = 0;
	boolean makeNewInterpolatedValues = false;
	
	int animNumber = 0;
	int playNumber = 0;

	public void settings() {
		size(600,600,P3D);
	}
	
	public void setup() {
		

		keyInfo = new KeyInfo();  

		mouseRecorder = new MouseRecorder(BASE_NAME+animNumber+EXTENSION);
		
		screenImage = new PImage32(width,height);
		//undoImage = new PImage32(screenImage.width,screenImage.height);
		screenImage.setAntialias(PImage32.ANTIALIAS_ON);
		shooterWorm = new ShooterWorm();

		helpScreen = new HelpScreen(this,keyInfo,shooterWorm);
		helpScreen.setVisible(true);
		
		
	}

	public void keyPressed() {
		if (!recording&&!playing&&!interpolating) {
			if (key==KeyInfo.HELP_SCREEN) {
				helpScreen.setVisible(!helpScreen.getVisible());
			}

			if (key==KeyInfo.WORM_ADD_SHOOTER) shooterWorm.addShooter();
			if (key==KeyInfo.WORM_DEL_SHOOTER) shooterWorm.removeShooter();
			if (key==KeyInfo.WORM_SHOW_HIDE) shooterWorm.setVisible(!shooterWorm.getVisible());
			if (key==KeyInfo.WORM_RANDOMIZE) shooterWorm.randomizePreset0();
			if (key==KeyInfo.WORM_RESET) shooterWorm.reset(mouseX,mouseY);
			if (key>=KeyInfo.WORM_PRESET_MIN&&key<=KeyInfo.WORM_PRESET_MAX) {
				shooterWorm.setPreset(key-49);
				helpScreen.setStatus("Changing to preset: "+(key-48));
			}


			if (key==KeyInfo.SCREEN_ANTIALIAS_OFF) {
				screenImage.setAntialias(PImage32.ANTIALIAS_OFF);
				helpScreen.setStatus("Antialias: OFF");
			}
			if (key==KeyInfo.SCREEN_ANTIALIAS_ON) {
				screenImage.setAntialias(PImage32.ANTIALIAS_ON);
				helpScreen.setStatus("Antialias: ON");
			}
			if (key==KeyInfo.SCREEN_CLEAR) {
				//undoImage.copyFrom(screenImage);
				screenImage.background(0,0,0);
			}
			/*if (key==KeyInfo.SCREEN_UNDO) {
				PImage32 tmp = new PImage32(screenImage.width,screenImage.height);
				tmp.copyFrom(screenImage);
				screenImage.copyFrom(undoImage);
				undoImage.copyFrom(tmp);
				helpScreen.setStatus("Undo");
			}*/
		}
		
		
		// not recording and not playing:
		
		if (!playing && !interpolating && key==KeyInfo.RECORDING_TOGGLE) {
			if (!recording) {
				recording = true;
				helpScreen.setVisible(false);
				//undoImage.copyFrom(screenImage);
				screenImage.background(0,0,0);
				mouseRecorder.setFilename(BASE_NAME+animNumber+EXTENSION);
				mouseRecorder.initRecording(shooterWorm);
			}
			
			else if (recording) {
				helpScreen.setVisible(false);
				recording = false;
				mouseRecorder.endRecording(this);
				animNumber++;
			}
		}
		
		if (!recording && !interpolating && (key==KeyInfo.PLAYING_TOGGLE || key==KeyInfo.CLEAR_AND_PLAYING_TOGGLE)) {
			if (!playing) {
				playing = true;
				if (key==KeyInfo.CLEAR_AND_PLAYING_TOGGLE) {
					//undoImage.copyFrom(screenImage);
					screenImage.background(0,0,0);
				}
				mouseRecorder.setFilename(BASE_NAME+playNumber+EXTENSION);
				shooterWorm = mouseRecorder.loadRecording(this);
				mouseRecorder.initPlaying();
				helpScreen.setVisible(false);
			}
			
			else if (playing) {
				playing = false;
				shooterWorm.setPressure(0);
			}
		}
		
		if (!playing && !recording && (key==KeyInfo.INTERPOLATE_RECORDINGS)) {
			if (!interpolating) {
				interpolating = true;
				makeNewInterpolatedValues = true;
				mouse1 = new MouseRecorder(BASE_NAME+0+EXTENSION);
				mouse2 = new MouseRecorder(BASE_NAME+1+EXTENSION);
				worm1 = mouse1.loadRecording(this);
				int nf1 = mouse1.getNumberOfFrames();
				System.out.println(nf1+" frames");
				worm2 = mouse2.loadRecording(this);
				int nf2 = mouse2.getNumberOfFrames();
				System.out.println(nf2+" frames");
				if (nf1<nf2) mouse2.setNumberOfFrames(nf1);
				else mouse1.setNumberOfFrames(nf2);
				
				currentInterpolationFrame = 0;
				helpScreen.setVisible(false);
				//undoImage.copyFrom(screenImage);
				screenImage.background(0,0,0);
			}
		}
		
		
	}

	public void mousePressed() {
		//undoImage.copyFrom(screenImage);
	}


	public void draw() {
		image(screenImage,0,0);
		
		if (interpolating && makeNewInterpolatedValues) {
			// make current shooter worm
			double percentage = (double)currentInterpolationFrame/INTERPOLATION_FRAMES;
			shooterWorm = ShooterWorm.getInterpolated(worm1,worm2,percentage);
			shooterWorm.setVisible(false);
			System.out.println(currentInterpolationFrame+" of "+INTERPOLATION_FRAMES);
			System.out.println(shooterWorm.toString());
			mouseRecorder = MouseRecorder.getInterpolated(mouse1,mouse2,percentage);
			makeNewInterpolatedValues = false;
		}
		
		if (!playing && !interpolating) {
			shooterWorm.draw(this,mouseX,mouseY,mousePressed,screenImage);
			screenImage.updatePixels();
		} else if (playing || interpolating) {
			shooterWorm.draw(this,mouseRecorder.getCurrentMouseX(),mouseRecorder.getCurrentMouseY(),mouseRecorder.getCurrentMousePressed(),screenImage);
			mouseRecorder.nextFrame();
			if (mouseRecorder.afterLastFrame()) {
				playing = false;
				if (!interpolating) helpScreen.setVisible(true);
				shooterWorm.setPressure(0);
				if (interpolating) {
					save(BASE_NAME+"WormDrawAnimatedFrame"+INTERPOLATION_FRAMES+"_"+nf(currentInterpolationFrame,5)+".tga");
					// clear screen
					//undoImage.copyFrom(screenImage);
					screenImage.background(0,0,0);
					currentInterpolationFrame++;
					makeNewInterpolatedValues = true;
					if (currentInterpolationFrame > INTERPOLATION_FRAMES) {
						makeNewInterpolatedValues = false;
						currentInterpolationFrame = 0;
						shooterWorm = new ShooterWorm();
						interpolating = false;
					}
				}
			}
		}
		
		
		
		if (recording) {
			helpScreen.setStatus("Recording "+mouseRecorder.getCurrentFrame());
			if (!mouseRecorder.record(mouseX, mouseY, mousePressed)) {
				recording = false;
				mouseRecorder.endRecording(this);
			}
		}
		
		helpScreen.update();
	}


}
