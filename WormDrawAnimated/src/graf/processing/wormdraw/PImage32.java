package graf.processing.wormdraw;

import processing.core.*;

/*
 *
 * uses sqrt anti-aliasing
 *
 */

public class PImage32 extends PImage {

	public static final boolean ANTIALIAS_OFF = false;
	public static final boolean ANTIALIAS_ON = true;
	private boolean ANTIALIAS = ANTIALIAS_OFF;

	float[][] pixels32;

	PImage32(int w, int h) {
		super(w,h);
		pixels32 = new float[w][h];
		loadPixels();
	}

	void setAntialias(boolean a) {
		ANTIALIAS = a;
	}
	
	
	// additive
	int mixColor(int c, double percd, int c2) {
		float perc = (float)percd;
		float r = perc*(c>>16&0xFF) + (c2>>16&0xFF);
		float g = perc*(c>>8&0xFF) + (c2>>8&0xFF);
		float b = perc*(c&0xFF) + (c2&0xFF);
		int rr = PApplet.min(PApplet.max((int)r,0),255);
		int gg = PApplet.min(PApplet.max((int)g,0),255);
		int bb = PApplet.min(PApplet.max((int)b,0),255);
		return 255<<24|rr<<16|gg<<8|bb;
	}

	void set(float x, float y, int c, float perc) {
		if (ANTIALIAS) {
			int x0 = (int)Math.floor(x);
			int x1 = x0 + 1;
			int y0 = (int)Math.floor(y);
			int y1 = y0 + 1;
			float px1 = x - x0;
			float px0 = 1.0f - px1;
			float py1 = y - y0;
			float py0 = 1.0f - py1;
			super.set(x0,y0,mixColor(c,Math.sqrt(px0*py0)*perc,super.get(x0,y0)));
			super.set(x0,y1,mixColor(c,Math.sqrt(px0*py1)*perc,super.get(x0,y1)));
			super.set(x1,y0,mixColor(c,Math.sqrt(px1*py0)*perc,super.get(x1,y0)));
			super.set(x1,y1,mixColor(c,Math.sqrt(px1*py1)*perc,super.get(x1,y1)));
		} 
		else {
			int bg = super.get((int)x,(int)y);
			super.set((int)x,(int)y,mixColor(c,perc,bg));
		}
	}

	void background(float r, float g, float b) {
		for (int x=0;x<this.width;x++) {
			for (int y=0;y<this.height;y++) {
				set(x,y,((int)r)<<16|((int)g)<<8|((int)b));
			}
		}
	}

	void copyFrom(PImage32 img) {
		img.loadPixels();
		for (int i=0;i<img.pixels.length;i++) {
			pixels[i] = img.pixels[i];
		}
		updatePixels();
	}

}
