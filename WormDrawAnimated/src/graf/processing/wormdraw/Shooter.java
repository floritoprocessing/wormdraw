package graf.processing.wormdraw;

import processing.core.*;

public class Shooter {
	
	public static final String VARNAME_X = "xp";
	public static final String VARNAME_Y = "yp";
	public static final String VARNAME_XM = "xm";
	public static final String VARNAME_YM = "ym";
	public static final String VARNAME_PRESSURE = "pressure";
	public static final String VARNAME_ACTIVE = "active";
	public static final String VARNAME_VISIBLE = "visible";

	private float x = 0;
	private float y = 0;
	private float xm = 0;
	private float ym = 0;
	private float pressure = 0.0f;

	private boolean ACTIVE = true;
	private boolean VISIBLE = true;
	
	public static final int TARGET_MOUSE = 0;
	public static final int TARGET_SHOOTER = 1;

	private Shooter SHOOTER;
	private int TARGET = 0;
		
	private ShooterWorm shooterWorm;

	Shooter(ShooterWorm shooterWorm) {
		this.shooterWorm = shooterWorm;
	}
	
	Shooter(ShooterWorm shooterWorm, float x, float y, float xm, float ym, float pressure, boolean ACTIVE, boolean VISIBLE) {
		this.shooterWorm = shooterWorm;
		this.x=x;
		this.y=y;
		this.xm=xm;
		this.ym=ym;
		this.pressure = pressure;
		this.ACTIVE = ACTIVE;
		this.VISIBLE = VISIBLE;
	}

	void setTarget(int t) {
		TARGET = t;
	}

	void setTarget(Shooter s) {
		TARGET = TARGET_SHOOTER;
		SHOOTER = s;
	}

	void setX(float x) {
		this.x=x;
	}
	
	void setY(float y) {
		this.y=y;
	}
	
	void setXM(float xm) {
		this.xm=xm;
	}
	
	void setYM(float ym) {
		this.ym=ym;
	}
	
	void setPressure(float pressure) {
		this.pressure = pressure;
	}

	void setActive() {
		ACTIVE = true;
	}

	boolean getActive() {
		return ACTIVE;
	}

	void setInactive() {
		ACTIVE = false;
	}
	
	boolean getVisible() {
		return VISIBLE;
	}

	void setVisible() {
		VISIBLE = true;
	}

	void setInvisible() {
		VISIBLE = false;
	}

	public void reset(int mouseX, int mouseY) {
		x = mouseX;
		y = mouseY;
		xm = 0;
		ym = 0;
		pressure = 0;
	}

	void update(int mouseX, int mouseY, boolean mousePressed) {
		float tx = 0, ty = 0;
		if (TARGET==TARGET_MOUSE) {
			tx = mouseX;
			ty = mouseY;
			pressure = shooterWorm.getPressureFollow()*(mousePressed?1.0f:0.0f) + (1.0f-shooterWorm.getPressureFollow())*pressure;
		} 
		else if (TARGET==TARGET_SHOOTER) {
			tx = SHOOTER.getX();
			ty = SHOOTER.getY();
			pressure = shooterWorm.getPressureFollow()*SHOOTER.getPressure() + (1.0f-shooterWorm.getPressureFollow())*pressure;
		}

		xm += (tx-x)*shooterWorm.getPull();
		ym += (ty-y)*shooterWorm.getPull();
		ym += shooterWorm.getGravity();
		xm *= (1-shooterWorm.getDrag());
		ym *= (1-shooterWorm.getDrag());
		x+=xm;
		y+=ym;
	}

	float getX() {
		return x;
	}

	float getY() {
		return y;
	}
	
	float getXM() {
		return xm;
	}
	
	float getYM() {
		return ym;
	}

	float getPressure() {
		return pressure;
	}

	private void show(PApplet pa) {
		if (ACTIVE) {
			pa.ellipseMode(PApplet.RADIUS);
			pa.stroke(0,0,255,160);
			pa.fill(255,0,0,32+pressure*128);
			pa.ellipse(x,y,3,3);
		}
	}

	void draw(PApplet pa, PImage32 img) {
		if (ACTIVE) {
			if (VISIBLE) show(pa);
			img.set(x,y,0xFF4628,shooterWorm.getHardness()*pressure); //color(255,70,40)
		}
	}
	
	
	
	public String toString() {
		String out = "" +
				VARNAME_X + " "+x+
				"," + VARNAME_Y + " "+y+
				"," + VARNAME_XM + " "+xm+
				"," + VARNAME_YM + " "+ym+
				"," + VARNAME_PRESSURE + " "+pressure+
				"," + VARNAME_ACTIVE + " "+(ACTIVE?1:0)+
				"," + VARNAME_VISIBLE + " "+(VISIBLE?1:0);
		return out;
	}
}
