class ShooterWorm {

  int PRESET = 0;
  int PRESET_AMOUNT = 6;

  int SHOOTER_AMOUNT_MAX = 200;

  int[] SHOOTER_AMOUNT = { 
    30 , 30 , 60 , 80 , 70 , 50       };
  float[] SHOOTER_PULL = { 
    0.05000, 0.03000, 0.33000, 0.40000, 0.80000, 0.56352    };
  float[] SHOOTER_DRAG = { 
    0.22000, 0.20000, 0.50000, 0.45000, 0.58000, 0.66743    };
  float[] SHOOTER_PRESSURE_FOLLOW = { 
    0.15000, 0.15000, 0.35000, 0.75000, 0.40000, 0.37751    };
  float[] SHOOTER_GRAVITY = {
    0.00000, 0.00000, 0.00000, 0.00000,-1.00000, 0.39951    };
  float[] SHOOTER_HARDNESS = {
    0.04000, 0.04000, 0.04000, 0.04000, 0.04000, 0.04000     };

  float amountMorph;
  float pullMorph;
  float dragMorph;
  float pressureFollowMorph;
  float gravityMorph;
  float hardnessMorph;

  float MORPH_STRENGTH = 0.03;

  private boolean VISIBLE = true;
  private boolean RUNNING = true;

  Shooter[] shooter;

  ShooterWorm() {

    shooter = new Shooter[SHOOTER_AMOUNT_MAX];
    for (int i=0;i<shooter.length;i++) {

      shooter[i] = new Shooter();
      shooter[i].setPull(SHOOTER_PULL[PRESET]);
      shooter[i].setDrag(SHOOTER_DRAG[PRESET]);
      shooter[i].setPressureFollow(SHOOTER_PRESSURE_FOLLOW[PRESET]);
      shooter[i].setGravity(SHOOTER_GRAVITY[PRESET]);
      shooter[i].setHardness(SHOOTER_HARDNESS[PRESET]);

      if (i<SHOOTER_AMOUNT[PRESET]) shooter[i].setActive();
      else shooter[i].setInactive();
      if (i==0) shooter[i].setTarget(Shooter.TARGET_MOUSE); 
      else shooter[i].setTarget(shooter[i-1]);

    }

    amountMorph = SHOOTER_AMOUNT[PRESET];
    pullMorph = SHOOTER_PULL[PRESET];
    dragMorph = SHOOTER_DRAG[PRESET];
    pressureFollowMorph = SHOOTER_PRESSURE_FOLLOW[PRESET];
    gravityMorph = SHOOTER_GRAVITY[PRESET];
    hardnessMorph = SHOOTER_HARDNESS[PRESET];
  }
  
  float getPull() {
    return pullMorph;
  }
  
  float getDrag() {
    return dragMorph;
  }
  
  float getGravity() {
    return gravityMorph;
  }
  
  float getHardness() {
    return hardnessMorph;
  }
  
  float getPressureFollow() {
    return pressureFollowMorph;
  }
  
  float getAmount() {
    return amountMorph;
  }
  
  void addShooter() {
    if (SHOOTER_AMOUNT[PRESET]<SHOOTER_AMOUNT_MAX) {
      SHOOTER_AMOUNT[PRESET]++;
      amountMorph++;
    }
  }
  
  void removeShooter() {
    if (SHOOTER_AMOUNT[PRESET]>2) {
      SHOOTER_AMOUNT[PRESET]--;
      amountMorph--;
    }
  }

  void randomizePreset0() {

    PRESET = 0;
    SHOOTER_AMOUNT[PRESET] = (int)random(SHOOTER_AMOUNT_MAX);
    SHOOTER_PULL[PRESET] = random(1.0);
    SHOOTER_DRAG[PRESET] = random(1.0);
    SHOOTER_PRESSURE_FOLLOW[PRESET] = random(1.0);
    SHOOTER_GRAVITY[PRESET] = random(-1.0,1.0);
    SHOOTER_HARDNESS[PRESET] = random(1.0);

    amountMorph = SHOOTER_AMOUNT[PRESET];
    pullMorph = SHOOTER_PULL[PRESET];
    dragMorph = SHOOTER_DRAG[PRESET];
    pressureFollowMorph = SHOOTER_PRESSURE_FOLLOW[PRESET];
    gravityMorph = SHOOTER_GRAVITY[PRESET];
    hardnessMorph = SHOOTER_HARDNESS[PRESET];

    println("Amount: "+amountMorph);
    println("Pull: "+pullMorph);
    println("Drag: "+dragMorph);
    println("PressureFollow: "+pressureFollowMorph);
    println("Gravity: "+gravityMorph);
    println("Hardness: "+hardnessMorph);
  }

  void setVisible(boolean v) {
    VISIBLE = v;
    for (int i=0;i<shooter.length;i++) if (VISIBLE) shooter[i].setVisible(); 
    else shooter[i].setInvisible();
  }

  boolean getVisible() {
    return VISIBLE;
  }

  void setRunning(boolean r) {
    RUNNING = r;
  }

  boolean getRunning() {
    return RUNNING;
  }

  void setPreset(int p) {
    PRESET = p;
  }

  void reset() {
    for (int i=0;i<shooter.length;i++) shooter[i].reset();
  }

  void morphPreset() {
    if (RUNNING) {

      int ps = PRESET;
      float newPull = SHOOTER_PULL[ps]*MORPH_STRENGTH + pullMorph*(1-MORPH_STRENGTH);
      float newDrag = SHOOTER_DRAG[ps]*MORPH_STRENGTH + dragMorph*(1-MORPH_STRENGTH);
      float newPressureFollow = SHOOTER_PRESSURE_FOLLOW[ps]*MORPH_STRENGTH + pressureFollowMorph*(1-MORPH_STRENGTH);
      float newGravity = SHOOTER_GRAVITY[ps]*MORPH_STRENGTH + gravityMorph*(1-MORPH_STRENGTH);
      float newAmount = SHOOTER_AMOUNT[ps]*MORPH_STRENGTH + amountMorph*(1-MORPH_STRENGTH);
      float newHardness = SHOOTER_HARDNESS[ps]*MORPH_STRENGTH + hardnessMorph*(1-MORPH_STRENGTH);

      for (int i=0;i<shooter.length;i++) {
        shooter[i].setPull(pullMorph);
        shooter[i].setDrag(dragMorph);
        shooter[i].setPressureFollow(pressureFollowMorph);
        shooter[i].setGravity(gravityMorph);
        shooter[i].setHardness(hardnessMorph);
        if (i<newAmount) {
          shooter[i].setActive();
        } 
        else {
          shooter[i].setInactive();
        }
      }

      pullMorph = newPull;
      dragMorph = newDrag;
      pressureFollowMorph = newPressureFollow;
      gravityMorph = newGravity;
      amountMorph = newAmount;
      hardnessMorph = newHardness;

    }
  }

  void draw(PImage32 img) {
    if (RUNNING) {
      for (int i=0;i<shooter.length;i++) shooter[i].update();
      for (int i=0;i<shooter.length;i++) shooter[shooter.length-i-1].draw(img);
    } 
    else {
      for (int i=0;i<shooter.length;i++) {
        shooter[shooter.length-i-1].show();
      }
    }
  }

}
