class HelpScreen {

  private boolean VISIBLE = false;
  private String FONT_LOAD_NAME = "CourierNewPSMT-11.vlw";
  private PFont font;
  private KeyInfo keyInfo;
  private ShooterWorm shooterWorm;

  private String STATUS = "";
  private static final int STATUS_TIME = 3000;
  private int statusTime = 0;


  HelpScreen(KeyInfo ki, ShooterWorm sw) {
    keyInfo = ki;
    shooterWorm = sw;
    font = loadFont(FONT_LOAD_NAME);
  }

  void setVisible(boolean v) {
    VISIBLE = v;
  }

  boolean getVisible() {
    return VISIBLE;
  }

  void setStatus(String s) {
    STATUS = s;
    statusTime = millis();
  }

  void showStatus() {
    if (!STATUS.equals("")) {
      // show:
      stroke(255,255,255);
      fill(128,128,128);
      rectMode(CORNER);
      rect(10,height-30,240,20);
      fill(0,0,0);
      textFont(font,11);
      text(STATUS,20,height-15);
      if (millis()>statusTime+STATUS_TIME) STATUS="";
    }
  }

  void update() {
    if (VISIBLE) {
      stroke(255,255,255);
      fill(128,128,128);
      rectMode(CORNER);
      rect(10,10,240,280);

      fill(0,0,0);
      textAlign(LEFT);
      textFont(font,11);
      text("KEY BINDINGS:",20,30);

      String[] asText = keyInfo.getAsText();
      for (int i=0;i<asText.length;i++) {
        text(asText[i],20,50+i*10);
      }
      
      text("SETTINGS:",20,70+asText.length*10);
      text("pull:           "+nf(shooterWorm.getPull(),1,5),20,70+asText.length*10+20);
      text("drag:           "+nf(shooterWorm.getDrag(),1,5),20,70+asText.length*10+30);
      text("gravity:        "+nf(shooterWorm.getGravity(),1,5),20,70+asText.length*10+40);
      text("hardness:       "+nf(shooterWorm.getHardness(),1,5),20,70+asText.length*10+50);
      text("pressureFollow: "+nf(shooterWorm.getPressureFollow(),1,5),20,70+asText.length*10+60);
      text("amount:         "+(int)shooterWorm.getAmount(),20,70+asText.length*10+70);
      
    }

    showStatus();
  }

}
