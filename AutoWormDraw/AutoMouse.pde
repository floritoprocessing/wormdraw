class AutoMouse {
  
  final float NEW_GESTURE_CHANCE = 0.1;
  
  Gesture lastGesture = null;
  Gesture currentGesture = null;
  //ArrayList<Gesture> gestures = new ArrayList<Gesture>();
  
  boolean lastDrawing = false;
  boolean drawing = false;
  
  void update() {
  
    if (currentGesture==null && random(1.0)<NEW_GESTURE_CHANCE) {
      
      println(nf(frameCount,5)+" New gesture");
      if (random(1.0)<0.05) 
        currentGesture = new Gesture(random(width),random(height));
      else if (lastGesture!=null) { 
        int i = (int)random(lastGesture.size());
        float x = lastGesture.get(i).x;
        float y = lastGesture.get(i).y;
        currentGesture = new Gesture(x,y);
      }
      mousePressed = random(1.0)<0.75;
      
    }
    
    if (currentGesture!=null) {
      
      PVector vec = currentGesture.get(currentGesture.size()-1);
      mouseX = (int)vec.x;
      mouseY = (int)vec.y;
      int result = currentGesture.move();
      if (result==1) {
        mousePressed = false;
        println(nf(frameCount,5)+" Done gesture");
        //gestures.add(currentGesture);
        lastGesture = currentGesture;
        currentGesture = null;
      }
      
    }
    
    
    
    
    //mouseX = (int)random(width);
  }
  
}
