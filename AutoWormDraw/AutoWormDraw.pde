
KeyInfo keyInfo;
ShooterWorm shooterWorm;
PImage32 screenImage, undoImage;
HelpScreen helpScreen;

AutoMouse autoMouse = new AutoMouse();

void setup() {
  //size(1920,1080);
  fullScreen();

  keyInfo = new KeyInfo();  

  screenImage = new PImage32(width,height);
  undoImage = new PImage32(screenImage.width,screenImage.height);
  screenImage.setAntialias(PImage32.ANTIALIAS_ON);
  shooterWorm = new ShooterWorm();
  
  helpScreen = new HelpScreen(keyInfo,shooterWorm);
  helpScreen.setVisible(true);
}

void keyPressed() {
  
  
  
  
  if (key==KeyInfo.HELP_SCREEN) {
    helpScreen.setVisible(!helpScreen.getVisible());
    shooterWorm.setRunning(!helpScreen.getVisible());
  }
  
  if (key==KeyInfo.WORM_ADD_SHOOTER) shooterWorm.addShooter();
  if (key==KeyInfo.WORM_DEL_SHOOTER) shooterWorm.removeShooter();
  if (key==KeyInfo.WORM_START_STOP) shooterWorm.setRunning(!shooterWorm.getRunning());
  if (key==KeyInfo.WORM_SHOW_HIDE) shooterWorm.setVisible(!shooterWorm.getVisible());
  if (key==KeyInfo.WORM_RANDOMIZE) shooterWorm.randomizePreset0();
  if (key==KeyInfo.WORM_RESET) shooterWorm.reset();
  if (key>=KeyInfo.WORM_PRESET_MIN&&key<=KeyInfo.WORM_PRESET_MAX) {
    shooterWorm.setPreset(key-49);
    helpScreen.setStatus("Changing to preset: "+(key-48));
  }
  
  
  if (key==KeyInfo.SCREEN_ANTIALIAS_OFF) {
    screenImage.setAntialias(PImage32.ANTIALIAS_OFF);
    helpScreen.setStatus("Antialias: OFF");
  }
  if (key==KeyInfo.SCREEN_ANTIALIAS_ON) {
    screenImage.setAntialias(PImage32.ANTIALIAS_ON);
    helpScreen.setStatus("Antialias: ON");
  }
  if (key==KeyInfo.SCREEN_CLEAR) {
    undoImage.copyFrom(screenImage);
    screenImage.background(0,0,0);
  }
  if (key==KeyInfo.SCREEN_UNDO) {
    PImage32 tmp = new PImage32(screenImage.width,screenImage.height);
    tmp.copyFrom(screenImage);
    screenImage.copyFrom(undoImage);
    undoImage.copyFrom(tmp);
    helpScreen.setStatus("Undo");
  }
}

void mousePressed() {
  undoImage.copyFrom(screenImage);
}


void draw() {
  for (int i=0;i<10;i++) {
  image(screenImage,0,0);

  // random key
  if (random(1.0)<0.00001) {
    println("Randomize preset");
    shooterWorm.randomizePreset0();
  }
  if (random(1.0)<0.0001) {
    int p = (int)random(6);
    println("Preset "+p);
    shooterWorm.setPreset(p);
  }
  autoMouse.update();

  //mouseX = (int)random(width);

  shooterWorm.morphPreset();
  shooterWorm.draw(screenImage);
  helpScreen.update();
  }
}
