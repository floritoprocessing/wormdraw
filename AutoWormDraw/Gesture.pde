class Gesture extends ArrayList<PVector> {
  
  int frame = 0;
  int duration = (int)random(60,400);
  float rd;
  float baseSpeed = random(0.1,3.0);
  float wigglyNess = random(radians(0.1),radians(25));
  float speed;
  
  Gesture(float x, float y) {
    super();
    println("\tbaseSpeed="+nf(baseSpeed,1,4));
    println("\twigglyNess="+nf(wigglyNess,1,4));
    add(new PVector(x,y));
    rd = random(TWO_PI);
    speed = baseSpeed;//random(0.1,0.5);
  }
  
  int move() {
    
    if (frame<duration) {
      float rdOff = random(wigglyNess);//radians(0.5),radians(3));
      rd += random(-rdOff,rdOff);
      float spdOff = random(baseSpeed*0.1);
      speed += random(-spdOff,spdOff);
      speed = max(0.1, min(3, speed));
      
      PVector prev = get(size()-1);
      
      float x = prev.x + cos(rd)*speed;
      float y = prev.y + sin(rd)*speed;
      if (x<0) x+=width;
      else if (x>=width) x-=width;
      if (y<0) y+=height;
      else if (y>=height) y-=height;
      
      PVector v = new PVector(x,y);
      add(v); 
      frame++;
      return 0;
    } else if (frame==duration) {
      return 1;
    } else {
      return 2;
    }
    
    
  }
  
}
