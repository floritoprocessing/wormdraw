/*
*
* uses sqrt anti-aliasing
*
*/

class PImage32 extends PImage {

  public static final boolean ANTIALIAS_OFF = false;
  public static final boolean ANTIALIAS_ON = true;
  private boolean ANTIALIAS = ANTIALIAS_OFF;

  float[][] pixels32;
  
  PImage32(int w, int h) {
    super(w,h);
    pixels32 = new float[w][h];
  }

  void setAntialias(boolean a) {
    ANTIALIAS = a;
  }

  // additive
  color mixColor(color c, float perc, color c2) {
    float r = perc*red(c) + red(c2);
    float g = perc*green(c) + green(c2);
    float b = perc*blue(c) + blue(c2);
    return color(r,g,b);
  }

  void set(float x, float y, color c, float perc) {
    if (ANTIALIAS) {
      int x0 = (int)floor(x);
      int x1 = x0 + 1;
      int y0 = (int)floor(y);
      int y1 = y0 + 1;
      float px1 = x - x0;
      float px0 = 1.0 - px1;
      float py1 = y - y0;
      float py0 = 1.0 - py1;
      super.set(x0,y0,mixColor(c,sqrt(px0*py0)*perc,super.get(x0,y0)));
      super.set(x0,y1,mixColor(c,sqrt(px0*py1)*perc,super.get(x0,y1)));
      super.set(x1,y0,mixColor(c,sqrt(px1*py0)*perc,super.get(x1,y0)));
      super.set(x1,y1,mixColor(c,sqrt(px1*py1)*perc,super.get(x1,y1)));
    } 
    else {
      color bg = super.get((int)x,(int)y);
      super.set((int)x,(int)y,mixColor(c,perc,bg));
    }
  }

  void background(float r, float g, float b) {
    for (int x=0;x<this.width;x++) {
      for (int y=0;y<this.height;y++) {
        set(x,y,color(r,g,b));
      }
    }
  }
  
  void copyFrom(PImage32 img) {
    for (int i=0;i<img.pixels.length;i++) {
      pixels[i] = img.pixels[i];
    }
  }

}
