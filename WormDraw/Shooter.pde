class Shooter {

  private float x = 0;
  private float y = 0;
  private float xm = 0;
  private float ym = 0;

  private float pull;
  private float drag;
  private float gravity;
  private float hardness;
  private float pressureFollow;

  public static final int TARGET_MOUSE = 0;
  public static final int TARGET_SHOOTER = 1;

  private Shooter SHOOTER;
  private int TARGET = 0;
  private float pressure = 0.0;

  private boolean ACTIVE = true;
  private boolean VISIBLE = true;

  Shooter() {
  }

  void setTarget(int t) {
    TARGET = t;
  }

  void setTarget(Shooter s) {
    TARGET = TARGET_SHOOTER;
    SHOOTER = s;
  }

  void setPull(float p) {
    pull = p;
  }

  float getPull() {
    return pull;
  }

  void setDrag(float d) {
    drag = d;
  }

  float getDrag() {
    return drag;
  }

  void setHardness(float h) {
    hardness = h;
  }

  float getHardness() {
    return hardness;
  }

  void setPressureFollow(float pf) {
    pressureFollow = pf;
  }

  float getPressureFollow() {
    return pressureFollow;
  }

  void setGravity(float g) {
    gravity = g;
  }

  float getGravity() {
    return gravity;
  }


  void setActive() {
    ACTIVE = true;
  }
  
  boolean getActive() {
    return ACTIVE;
  }

  void setInactive() {
    ACTIVE = false;
  }

  void setVisible() {
    VISIBLE = true;
  }

  void setInvisible() {
    VISIBLE = false;
  }

  void reset() {
    x = mouseX;
    y = mouseY;
    xm = 0;
    ym = 0;
    pressure = 0;
  }

  void update() {
    float tx = 0, ty = 0;
    if (TARGET==TARGET_MOUSE) {
      tx = mouseX;
      ty = mouseY;
      pressure = pressureFollow*(mousePressed?1.0:0.0) + (1.0-pressureFollow)*pressure;
    } 
    else if (TARGET==TARGET_SHOOTER) {
      tx = SHOOTER.getX();
      ty = SHOOTER.getY();
      pressure = pressureFollow*SHOOTER.getPressure() + (1.0-pressureFollow)*pressure;
    }

    xm += (tx-x)*pull;
    ym += (ty-y)*pull;
    ym += gravity;
    xm *= (1-drag);
    ym *= (1-drag);
    x+=xm;
    y+=ym;
  }

  float getX() {
    return x;
  }

  float getY() {
    return y;
  }

  float getPressure() {
    return pressure;
  }

  void show() {
    if (ACTIVE) {
      ellipseMode(CENTER);
      stroke(0,0,255,160);
      fill(255,0,0,32+pressure*128);
      ellipse(x,y,3,3);
    }
  }

  void draw(PImage32 img) {
    if (ACTIVE) {
      if (VISIBLE) show();
      img.set(x,y,color(255,70,40),hardness*pressure);
    }
  }
}
