class KeyInfo {

  public static final char HELP_SCREEN = 'h';
  
  public static final char WORM_START_STOP = 's';
  public static final char WORM_SHOW_HIDE = 'w';
  public static final char WORM_RANDOMIZE = 'r';
  public static final char WORM_RESET = 'R';
  public static final char WORM_PRESET_MIN = '1';
  public static final char WORM_PRESET_MAX = '6';
  public static final char WORM_ADD_SHOOTER = '+';
  public static final char WORM_DEL_SHOOTER = '-';
  
  public static final char SCREEN_ANTIALIAS_OFF = 'a';
  public static final char SCREEN_ANTIALIAS_ON = 'A';
  public static final char SCREEN_CLEAR = 'C';
  public static final char SCREEN_UNDO = 'u';

  private String[] asText = {
    "[h] Helpscreen on/off",
    "",
    "[1]..[6] Select preset",
    "",
    "[s] to start/stop running",
    "[r] to randomize preset 1",
    "[w] to show/hide worm",
    "[R] to reset worm",
    "[+]/[-] to add/delete elements",
    "",
    "[C] to clear screen",
    "[u] to undo drawing",
    "[a] Antialias off",
    "[A] Antialias on"
  };

  KeyInfo() {

  }

  String[] getAsText() {
    return asText;
  }

}
